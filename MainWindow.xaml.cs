﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ChessRatingLists
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            var grid = ((DataGrid)this.FindName("dataset"));
            var progress = (ProgressBar)this.FindName("Progress");

            new System.Threading.Thread(new System.Threading.ThreadStart(() =>
                {

                    //get data from internets
                    var uri = new Uri("http://ratings.fide.com/top.phtml?list=men");

                    var request = System.Net.WebRequest.Create(uri);

                    AddProgress(25, progress);

                    var response = request.GetResponse();

                    if (response == null)
                        return;

                    AddProgress(25, progress);

                    var stream = response.GetResponseStream();

                    var reader = new System.IO.StreamReader(stream);

                    var store = new Store();

                    reader.ReadLine(); reader.ReadLine();
                    var str = reader.ReadToEnd().Trim();

                    AddProgress(25, progress);

                    // Due to a ill formed webpages, instead of parsing with an XML parser; we use
                    // regex and assume the page will not change format too much.
                    // Additionally we assume the individual player pages are of the same format as each other.

                    string[] a = System.Text.RegularExpressions.Regex.Split(str, "<table([^>.]*)>|</table>");

                    var body = a[11];

                    AddProgress(25, progress);

                    string[] b = System.Text.RegularExpressions.Regex.Split(body, "<td>|</td>");

                    int start = 16;
                    int next = 13;
                    int current = start;

                    lock (grid)
                    {

                        while (current < b.Length)
                        {

                            Player player = new Player();

                            string[] name = System.Text.RegularExpressions.Regex.Split(b[current], ">|<");

                            player.Name = name[2];

                            string[] link = System.Text.RegularExpressions.Regex.Split(b[current], "=| ");

                            new System.Threading.Thread(new System.Threading.ParameterizedThreadStart((o) =>
                            {

                                var g = (DataGrid)((object[])o)[0];
                                var p = (Player)((object[])o)[1];
                                var l = "http://ratings.fide.com/card.phtml?event=" + (string)((object[])o)[2];

                                var req = System.Net.WebRequest.Create(new Uri(l));

                                var res = req.GetResponse();

                                if (res == null)
                                    return;

                                var s = res.GetResponseStream();

                                var read = new System.IO.StreamReader(s);

                                var entry = read.ReadToEnd();

                                var rapid = System.Text.RegularExpressions.Regex.Split(entry, "<|>");

                                player.Rapid = rapid[690];
                                player.Blitz = rapid[704];

                                grid.Dispatcher.BeginInvoke(new System.Action<DataGrid, Store>(RefreshGrid), new object[] { grid, store });

                                AddProgress(1, progress);

                                res.Close();

                            })).Start(new object[] { grid, player, link[3] }); 

                            string[] rating = System.Text.RegularExpressions.Regex.Split(b[current + 6], ";");

                            player.Standard = rating[1];

                            store.AddPlayer(player);
                            
                            grid.Dispatcher.BeginInvoke(new System.Action<DataGrid, Store>(RefreshGrid), new object[] { grid, store });
                            
                            current += next;
                        }

                    }

                    stream.Close();
                    response.Close();

                })).Start();

        }

        /// <summary>
        /// Adds progess to the progress bar.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="progress"></param>
        private void AddProgress(double value, ProgressBar progress)
        {
            progress.Dispatcher.BeginInvoke(new System.Action<ProgressBar>((bar) => { ((ProgressBar)bar).Value+=value; }), progress);
        }

        /// <summary>
        /// Refreshes the data view grid control.
        /// </summary>
        /// <param name="grid"></param>
        /// <param name="store"></param>
        private void RefreshGrid(DataGrid grid, Store store)
        {
            lock (grid)
            {
                grid.ItemsSource = store.Data();
            }
        }

        /// <summary>
        /// The details of a player.
        /// </summary>
        private class Player
        {
            public String Name { get; set; }
            public String Standard { get; set; }
            public String Rapid { get; set; }
            public String Blitz { get; set; }
        }

        /// <summary>
        /// The back store for the player database.
        /// </summary>
        private class Store
        {

            List<Player> players;

            public Store()
            {
                players = new List<Player>();
            }

            public void AddPlayer(Player p)
            {
                players.Add(p);
            }

            public List<Player> Data()
            {
                return new List<Player>(players);
            }
        }

    }
}
